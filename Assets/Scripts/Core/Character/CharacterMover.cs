﻿using UnityEngine;

namespace Core.Character
{
    [System.Serializable]
    public class FootSteps
    {
        [SerializeField]
        private AudioSource _audioSource = null;
        [SerializeField]
        private AudioClip _footStepSound = null;
        [SerializeField]
        private Animator _anim = null;
        [SerializeField]
        private float _rayLength = 0.075f;

        private bool _lFootOnGround = true;
        private bool _lFootOnGroundOld = true;

        private bool _rFootOnGround = true;
        private bool _rFootOnGroundOld = true;

        private Ray _ray = new Ray();

        public void PlayAudio()
        {
            _ray.direction = Vector3.down;

            _ray.origin = _anim.GetBoneTransform(HumanBodyBones.LeftFoot).position;
            _lFootOnGround = Physics.Raycast(_ray, _rayLength);

            _ray.origin = _anim.GetBoneTransform(HumanBodyBones.RightFoot).position;
            _rFootOnGround = Physics.Raycast(_ray, _rayLength);

            if (_lFootOnGround && !_lFootOnGroundOld)
                _audioSource.PlayOneShot(_footStepSound);
            if (_rFootOnGround && !_rFootOnGroundOld)
                _audioSource.PlayOneShot(_footStepSound);

            _lFootOnGroundOld = _lFootOnGround;
            _rFootOnGroundOld = _rFootOnGround;
        }
    }

    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CharacterStats))]
    public class CharacterMover : MonoBehaviour
    {
        [System.Serializable]
        private class MovingInputParams
        {
            public string Vert = "Vert";
            public string Hor = "Hor";
            public string State = "State";
            public string IsRot = "isRot";
            public string Rot = "Rot";
            public string MoveSpeed = "MoveSpeed";
        }

        [System.Serializable]
        private class Moving
        {
            private MovingInputParams _inputParams = null;

            private Animator _anim = null;

            private float _sens = 15f;
            private float _luft = 60f;

            private bool _isRotation = false;

            public Moving(Animator anim, CharacterStats.Move moveStats, MovingInputParams inputParams)
            {
                _anim = anim;
                _sens = moveStats.AnimSens;
                _luft = moveStats.RotationLuft;

                _inputParams = inputParams;
            }

            public void Move(Transform bodyTrans, Vector3 targetTransPos, Vector3 moveAxis, float deltaTime, float lookAtWeight = 1f)
            {
                _anim.SetFloat(_inputParams.Vert, moveAxis.z, 1f / _sens, deltaTime);
                _anim.SetFloat(_inputParams.Hor, moveAxis.x, 1f / _sens, deltaTime);

                _anim.SetLookAtWeight(lookAtWeight, 0.7f, 0.9f, 1f, 1f);
                _anim.SetLookAtPosition(targetTransPos);
            }

            public void Rot(Transform bodyTrans, Vector3 targetTransPos, bool isRot, float deltaTime)
            {
                var oldRot = bodyTrans.eulerAngles;
                bodyTrans.LookAt(targetTransPos);

                var angleBetween = Mathf.DeltaAngle(bodyTrans.eulerAngles.y, oldRot.y);
                _anim.SetFloat(_inputParams.Rot, (angleBetween < 0) ? 1 : -1, 1f / _sens, deltaTime);

                angleBetween = Mathf.Abs(angleBetween);

                deltaTime *= _sens;

                if (!isRot)
                    oldRot.y = Mathf.LerpAngle(oldRot.y, bodyTrans.eulerAngles.y, deltaTime);
                else if (angleBetween > _luft)
                    _isRotation = true;

                bodyTrans.eulerAngles = oldRot;

                if (!_isRotation)
                    return;

                if (angleBetween * Mathf.Deg2Rad <= deltaTime)
                    _isRotation = isRot = false;

                _anim.SetBool(_inputParams.IsRot, isRot);
            }

            public void SetState(float newState, float deltaTime)
            {
                _anim.SetFloat(_inputParams.State, newState, 1f / _sens, deltaTime);
            }
        }

        [SerializeField]
        private MovingInputParams _inputParams = null;

        private float _lookAtWeight = 1f;

        private Transform _selfTrans = null;
        private Animator _anim = null;

        private CharacterStats _charStats = null;

        private Moving _moving = null;

        private bool _isSimpleRot = false;

        private Vector3 _moveInput = Vector3.zero;
        private Vector3 _targetPos = Vector3.zero;
        private float _state = 0f;

        public void SetLookAtWeight(float value)
        {
            _lookAtWeight = value;
        }

        private void Start()
        {
            _selfTrans = transform;
            _anim = GetComponent<Animator>();

            _charStats = GetComponent<CharacterStats>();
            var moveStats = _charStats.MoveStats;
            _moving = new Moving(_anim, moveStats, _inputParams);
        }

        public void SetState(float state)
        {
            _state = state;
        }

        public void SetInputs(Vector3 moveInput, Vector3 targetPos, bool isSimpleRot)
        {
            _moveInput = moveInput;
            _targetPos = targetPos;
            _isSimpleRot = isSimpleRot;
        }

        private void OnAnimatorIK()
        {
            _anim.SetFloat(_inputParams.MoveSpeed, _charStats.MoveStats.MoveSpeed);

            _moving.SetState(_state, Time.deltaTime);

            _moving.Move(_selfTrans, _targetPos, _moveInput, Time.deltaTime, _lookAtWeight);
            var isRot = _isSimpleRot ? false : Mathf.Abs(_moveInput.x) < Mathf.Epsilon && Mathf.Abs(_moveInput.z) < Mathf.Epsilon;
            _moving.Rot(_selfTrans, _targetPos, isRot, Time.deltaTime);
        }

        public Vector3 GetTargetPos()
        {
            return _targetPos;
        }
    }
}

