﻿using UnityEngine;
using UnityEngine.AI;

namespace Core.Character
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CharacterStats))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class CharacterDier : MonoBehaviour
    {
        private string _impactInputParam = "isStoped";

        private Animator _anim = null;
        private CharacterStats _charStats = null;
        private NavMeshAgent _navAgent = null;
        private Collider _collider = null;

        private Rigidbody[] _rigidBodyes = new Rigidbody[0];

        private void Init()
        {
            _rigidBodyes = transform.root.GetComponentsInChildren<Rigidbody>();
            _anim = GetComponent<Animator>();
            _charStats = GetComponent<CharacterStats>();
            _navAgent = GetComponent<NavMeshAgent>();
            _collider = GetComponent<Collider>();
        }

        private void OnEnable()
        {
            Init();
            _charStats.OnDamaged.AddListener(Damaged);
            _charStats.OnDie.AddListener(Die);
            SetLivingState(true);
        }

        private void OnDisable()
        {
            _charStats.OnDamaged.RemoveListener(Damaged);
            _charStats.OnDie.RemoveListener(Die);
        }

        [ContextMenu("Damaged")]
        public void Damaged()
        {
            _anim.SetTrigger(_impactInputParam);
        }

        [ContextMenu("Die")]
        public void Die()
        {
            SetLivingState(false);
        }

        private void SetLivingState(bool state)
        {
            _anim.enabled = state;
            _navAgent.enabled = state;
            _collider.enabled = state;

            for (int i = 0; i < _rigidBodyes.Length; i++)
                _rigidBodyes[i].isKinematic = state;

            if (!state)
            {
                var weapon = GetComponentInChildren<Weapons.Weapon>();
                if (weapon != null)
                    weapon.DropWeapon();
            }
        }
    }
}