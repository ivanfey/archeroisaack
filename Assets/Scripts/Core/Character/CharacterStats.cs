﻿using UnityEngine;
using UnityEngine.Events;

namespace Core.Character
{
    public interface IDamageable
    {
        void Damage(float damage, GameObject sender);
    }

    [RequireComponent(typeof(Animator))]
    public class CharacterStats : MonoBehaviour, IDamageable
    {
        [System.Serializable]
        public class Main
        {
            public float MaxHealth = 100f;
            public float Health = 100f;

            public float GetInhHealth()
            {
                return Health / MaxHealth;
            }
        }

        [System.Serializable]
        public class Move
        {
            public float AnimSens = 7f;
            public float RotationLuft = 60f;
            public float MoveSpeed = 2f;
        }

        public Main MainStats = new Main();
        public Move MoveStats = new Move();
        
        public UnityEvent OnDamaged = new UnityEvent();
        public UnityEvent OnDie = new UnityEvent();

        public void Damage(float damage, GameObject sender)
        {
            if (MainStats.Health <= Mathf.Epsilon)
                return;

            MainStats.Health -= damage;

            OnDamaged.Invoke();

            if (MainStats.Health <= Mathf.Epsilon)
                OnDie.Invoke();
        }
    }
}

