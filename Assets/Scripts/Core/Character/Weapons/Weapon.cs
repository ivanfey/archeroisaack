﻿using System.Collections;
using UnityEngine;

namespace Core.Character.Weapons
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField]
        protected GameObject _dropedWeapon = null;
        [SerializeField]
        private float _lookAtWeight = 1f;

        protected Animator _anim = null;
        protected CharacterAttacker _attacker = null;
        protected CharacterMover _mover = null;

        protected IEnumerator Init()
        {
            _anim = GetComponentInParent<Animator>();
            while (_anim == null)
                yield return null;

            _attacker = GetComponentInParent<CharacterAttacker>();
            while (_attacker == null)
                yield return null;

            _mover = GetComponentInParent<CharacterMover>();
            while (_mover == null)
                yield return null;

            _mover.SetLookAtWeight(_lookAtWeight);
            var otherWeapons = _anim.GetComponentsInChildren<Weapon>();
            for (int i = 0; i < otherWeapons.Length; i++)
            {
                if (GetInstanceID() != otherWeapons[i].GetInstanceID())
                {
                    otherWeapons[i].DropWeapon();
                }
            }
        }

        [ContextMenu("Drop weapon")]
        public void DropWeapon()
        {
            if (_dropedWeapon == null)
                return;

            _attacker.SetEmptyWeapon();
            
            Instantiate(_dropedWeapon, transform.position, transform.rotation);

            Destroy(gameObject);
        }
    }
}