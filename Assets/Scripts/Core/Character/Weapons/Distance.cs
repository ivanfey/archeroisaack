﻿using System.Collections;
using UnityEngine;

namespace Core.Character.Weapons
{
    public class Distance : Weapon
    {
        [SerializeField]
        private Shoot _shoot = null;

        private IEnumerator Start()
        {
            yield return Init();

            var aimer = GetComponentInParent<CharacterAimer>();
            while (aimer == null)
                yield return null;

            transform.SetParent(aimer.GetWeaponPoint());
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            _shoot.SetStartingParams(transform.localPosition, transform.localEulerAngles);
            _attacker.SetNewWeapon(_shoot);
        }
    }
}

