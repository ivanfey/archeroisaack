﻿using System.Collections;
using UnityEngine;

namespace Core.Character.Weapons
{
    public class LongBow : Weapon
    {
        [SerializeField]
        private Character.LongBow _longBow = null;
        [SerializeField]
        private HumanBodyBones _boneForWeapon = HumanBodyBones.LeftHand;

        private CharacterAimer _charAimer = null;

        private IEnumerator Start()
        {
            yield return Init();

            _charAimer = GetComponentInParent<CharacterAimer>();
            while (_charAimer == null)
                yield return null;

            _longBow.SetShootPoint(_charAimer.GetWeaponPoint().transform);

            _attacker.SetNewWeapon(_longBow);

            transform.SetParent(_anim.GetBoneTransform(_boneForWeapon));

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}