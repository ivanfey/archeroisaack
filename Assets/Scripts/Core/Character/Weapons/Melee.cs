﻿using System.Collections;
using UnityEngine;

namespace Core.Character.Weapons
{
    public class Melee : Weapon
    {
        [SerializeField]
        private Attack _attack = null;
        [SerializeField]
        private HumanBodyBones _boneForWeapon = HumanBodyBones.RightHand;

        private IEnumerator Start()
        {
            yield return Init();

            _attacker.SetNewWeapon(_attack);

            transform.SetParent(_anim.GetBoneTransform(_boneForWeapon));

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}