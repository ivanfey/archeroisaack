﻿using UnityEngine;

namespace Core.Character.InputSpace
{
    [System.Serializable]
    public class TopDownCameraInput
    {
        private const float _camRayDistance = 100f;

        [SerializeField]
        private Vector3 _camOffsetDefault = new Vector3(0, 10f, -10f);
        [SerializeField]
        private Vector3 _camOffsetTop = new Vector3(0, 10f, 0f);
        [SerializeField]
        private float _camSpeed = 5f;
        [SerializeField]
        private LayerMask _wallLayer = Physics.AllLayers;

        private Vector3 _camOffset = Vector3.zero;

        private Transform _originTrans = null;
        private Transform _camTrans = null;

        public void Init(Transform originTrans, Transform camTrans)
        {
            _originTrans = originTrans;
            _camTrans = camTrans;
        }

        public void PosChange(float deltaTime)
        {
            var camSpeed = deltaTime * _camSpeed;

            var origin = _originTrans.position + _camOffsetDefault;
            var heading = _originTrans.position - origin;
            var distance = heading.magnitude;
            var direction = heading / distance;
            var isWall = Physics.Raycast(origin, direction, _camRayDistance, _wallLayer);
            _camOffset = Vector3.Lerp(_camOffset, isWall ? _camOffsetTop : _camOffsetDefault, camSpeed);
            var offset = _originTrans.position + _camOffset;

            _camTrans.position = Vector3.Lerp(_camTrans.position, offset, camSpeed);

            var oldCamRot = _camTrans.eulerAngles;
            _camTrans.LookAt(_originTrans);
            var newCamRot = _camTrans.eulerAngles;
            newCamRot.x = Mathf.LerpAngle(oldCamRot.x, newCamRot.x, camSpeed);
            newCamRot.y = 0;
            newCamRot.z = Mathf.LerpAngle(oldCamRot.z, newCamRot.z, camSpeed);
            _camTrans.eulerAngles = newCamRot;
        }
    }
    [System.Serializable]
    public class TopDownMovingInput
    {
        [SerializeField]
        private string _moveVertInput = "Vertical";
        [SerializeField]
        private string _moveHorInput = "Horizontal";
        [SerializeField]
        private float _targetSpeed = 5f;
        [SerializeField]
        private float _targetDistance = 15f;
        [SerializeField]
        private float _findTargetsRadius = 10f;

        private Transform _bodyTrans = null;

        private Vector3 _moveInput = Vector3.zero;
        private Vector3 _targetPos = Vector3.zero;

        public void Init(Transform bodyTrans)
        {
            _bodyTrans = bodyTrans;
        }

        public void SetInputs(float deltaTime, out Vector3 moveInput, out Vector3 targetPos, out bool findEnemy)
        {
            var angle = _bodyTrans.eulerAngles.y * Mathf.Deg2Rad;
            var cosAngle = Mathf.Cos(angle);
            var sinAngle = Mathf.Sin(angle);
            var vertInput = SimpleInput.GetAxis(_moveVertInput);
            var horInput = SimpleInput.GetAxis(_moveHorInput);

            var hor = cosAngle * horInput - sinAngle * vertInput;
            var vert = cosAngle * vertInput + sinAngle * horInput;

            _moveInput.x = hor;
            _moveInput.y = 0;
            _moveInput.z = vert;

            var minDistance = Mathf.Infinity;
            var isInput = Mathf.Abs(horInput) > 0.1f || Mathf.Abs(vertInput) > 0.1f;
            var deltaPos = isInput ? new Vector3(horInput, 0, vertInput) * _targetDistance : _bodyTrans.forward * _targetDistance;
            var tempTargetPos = _bodyTrans.position + deltaPos;

            findEnemy = false;
            var botInputs = BotInput.AllBots; 
            for (int i = 0; i < botInputs.Count; i++)
            {
                var anim = botInputs[i].GetAnim();
                if (anim == null || !anim.enabled)
                    continue;

                var distance = Vector3.SqrMagnitude(botInputs[i].transform.position - _bodyTrans.position);

                if (distance >= _findTargetsRadius * _findTargetsRadius)
                    continue;

                if (distance >= minDistance)
                    continue;

                minDistance = distance;
                tempTargetPos = anim.GetBoneTransform(HumanBodyBones.Chest).position;
                findEnemy = true;
            }
            _targetPos = Vector3.Lerp(_targetPos, tempTargetPos, deltaTime * _targetSpeed);

            moveInput = _moveInput;
            targetPos = _targetPos;
        }
    }
    [System.Serializable]
    public class AttackInput
    {
        [SerializeField]
        private string _attackInput = "Fire1";

        private bool _isAttack = false;

        public void SetInputs(out bool isAttack)
        {
            _isAttack = SimpleInput.GetButton(_attackInput);
            isAttack = _isAttack;
        }
    }
}

