﻿using UnityEngine;

namespace Core.Character.InputSpace
{
    [RequireComponent(typeof(CharacterMover))]
    [RequireComponent(typeof(CharacterAttacker))]
    public class PlayerInput : MonoBehaviour
    {
        public static PlayerInput Instance = null;

        [SerializeField]
        private TopDownMovingInput _topDownMoving = null;
        [SerializeField]
        private AttackInput _attack = null;
        [SerializeField]
        private TopDownCameraInput _topDownCamera = null;

        private CharacterMover _charMover = null;
        private CharacterAttacker _charAttacker = null;
        private Animator _anim = null;

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        private void Start()
        {
            _charMover = GetComponent<CharacterMover>();
            _charAttacker = GetComponent<CharacterAttacker>();
            _anim = GetComponent<Animator>();

            var bodyTrans = transform;
            var cam = Camera.allCameras[0];
            var camTrans = cam.transform;

            _topDownMoving.Init(bodyTrans);
            _topDownCamera.Init(bodyTrans, camTrans);
        }

        public Animator GetAnim()
        {
            return _anim;
        }

        private void Update()
        {
            var deltaTime = Time.deltaTime;

            var moveInput = Vector3.zero;
            var targetPos = Vector3.zero;
            var isAttack = false;
            var isSimpleRot = true;
            var findEnemy = false;

            _topDownMoving.SetInputs(deltaTime, out moveInput, out targetPos, out findEnemy);
            _topDownCamera.PosChange(deltaTime);

            moveInput *= 1.5f;
            _charMover.SetInputs(moveInput, targetPos, isSimpleRot);

            //_attack.SetInputs(out isAttack);
            if (Mathf.Abs(moveInput.x) + Mathf.Abs(moveInput.z) < 0.2f && findEnemy)
                isAttack = true;

            _charAttacker.SetInputs(isAttack);
        }
    }
}

