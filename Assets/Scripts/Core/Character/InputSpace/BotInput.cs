﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Core.Character.InputSpace
{
    [RequireComponent(typeof(CharacterMover))]
    [RequireComponent(typeof(CharacterAttacker))]
    public class BotInput : MonoBehaviour
    {
        private enum AttackStyle
        {
            Melle,
            Distance
        }

        public static List<BotInput> AllBots = new List<BotInput>();

        [SerializeField]
        private AttackStyle _style = AttackStyle.Melle;

        [SerializeField]
        private float _agrDistance = 10f;

        [SerializeField]
        private float _melleMoveDistance = 2.0f;
        [SerializeField]
        private float _melleAttackDistance = 2.5f;

        [SerializeField]
        private float _distanceMoveDistance = 5.0f;
        [SerializeField]
        private float _distanceAttackDistance = 7.5f;

        private float _moveDistance = 0f;
        private float _attackDistance = 0f;

        private static Transform _playerChest = null;

        private Transform _selfTrans = null;
        private CharacterMover _charMover = null;
        private CharacterAttacker _charAttacker = null;
        private NavMeshAgent _navAgent = null;
        private Animator _anim = null;
        private CharacterStats _charStats = null;

        private bool _initDone = false;

        private void OnEnable()
        {
            if (!AllBots.Contains(this))
                AllBots.Add(this);
        }

        private void OnDisable()
        {
            if (AllBots.Contains(this))
                AllBots.Remove(this);
        }

        private IEnumerator Start()
        {
            while (_playerChest == null)
            {
                yield return null;

                var playerAnim = PlayerInput.Instance.GetAnim();
                if (playerAnim == null)
                    continue;

                _playerChest = playerAnim.GetBoneTransform(HumanBodyBones.Chest);
            }

            _selfTrans = transform;
            _charMover = GetComponent<CharacterMover>();
            _charAttacker = GetComponent<CharacterAttacker>();
            _anim = GetComponent<Animator>();
            _navAgent = GetComponent<NavMeshAgent>();
            _charStats = GetComponent<CharacterStats>();

            _initDone = true;
        }

        public Animator GetAnim()
        {
            return _anim;
        }

        private void LateUpdate()
        {
            if (!_initDone)
                return;

            if (!_navAgent.enabled)
                return;

            var deltaTime = Time.deltaTime;

            switch (_style)
            {
                case AttackStyle.Melle:
                    _moveDistance = _melleMoveDistance;
                    _attackDistance = _melleAttackDistance;
                    break;
                case AttackStyle.Distance:
                    _moveDistance = _distanceMoveDistance;
                    _attackDistance = _distanceAttackDistance;
                    break;
            }

            var heading = PlayerInput.Instance.GetAnim().transform.position - transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;

            _navAgent.isStopped = distance >= _agrDistance;
            _navAgent.speed = _charStats.MoveStats.MoveSpeed;
            _navAgent.SetDestination(PlayerInput.Instance.GetAnim().transform.position - direction * _moveDistance);

            var isApplyRootMotion = false;
            if (_anim.GetCurrentAnimatorStateInfo(0).IsTag("Move"))
                isApplyRootMotion = true;
            if (_anim.GetCurrentAnimatorStateInfo(1).IsTag("Move"))
                isApplyRootMotion = true;
            if (_anim.GetNextAnimatorStateInfo(0).IsTag("Move"))
                isApplyRootMotion = true;
            if (_anim.GetNextAnimatorStateInfo(1).IsTag("Move"))
                isApplyRootMotion = true;
            _anim.applyRootMotion = isApplyRootMotion;
            if(isApplyRootMotion)
                _navAgent.isStopped = true;

            var moveInput = transform.InverseTransformDirection(_navAgent.velocity);
            var targetPos = _playerChest.position;
            _charMover.SetInputs(moveInput, targetPos, true);

            var isAttack = false;
            isAttack = distance < _attackDistance;
            _charAttacker.SetInputs(isAttack);
        }
    }
}