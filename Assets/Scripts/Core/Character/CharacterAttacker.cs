﻿using UnityEngine;

namespace Core.Character
{
    public interface IAttackable
    {
        void Attacking(Animator anim, bool isAttack);
        void Hit(Animator anim, GameObject damager);
        void SetState(CharacterMover mover);
    }

    [System.Serializable]
    public class Attack: IAttackable
    {
        [System.Serializable]
        private class AttackInputParams
        {
            public string AttackSpeedInput = "AttackSpeed";
            public string AttackInput_0 = "isAttack_0";
            public string AttackInput_1 = "isAttack_1";
            public string AttackInput_2 = "isAttack_2";
            public string AttackInput_3 = "isAttack_3";
            public string NextStepInput = "NextStep";
            public string ComboID = "ComboID";
        }

        [SerializeField]
        private AttackInputParams _inputParams = null;

        [SerializeField]
        private float _animState = 0f;
        [SerializeField]
        private LayerMask _mask = Physics.AllLayers;
        [SerializeField]
        private float _attackLength = 1.5f;
        [SerializeField]
        private float _attackWidth = 0.25f;
        [SerializeField]
        private float _attackSpeed = 1f;
        [SerializeField]
        private float _damage = 10f;
        [SerializeField]
        private int _comboID = 0;

        public void Attacking(Animator anim, bool isAttack)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);

            anim.SetInteger(_inputParams.ComboID, _comboID);

            anim.SetFloat(_inputParams.AttackSpeedInput, _attackSpeed);

            var nextStep = isAttack && anim.GetFloat(_inputParams.NextStepInput) > Mathf.Epsilon;

            anim.SetBool(_inputParams.AttackInput_0, isAttack);

            anim.SetBool(_inputParams.AttackInput_1, nextStep);
            anim.SetBool(_inputParams.AttackInput_2, nextStep);
            anim.SetBool(_inputParams.AttackInput_3, nextStep);
        }

        public void Hit(Animator anim, GameObject damager)
        {
            var headTrans = anim.GetBoneTransform(HumanBodyBones.Head);
            var ray = new Ray(headTrans.position, headTrans.forward);
            var hits = Physics.SphereCastAll(ray, _attackWidth, _attackLength, _mask, QueryTriggerInteraction.Ignore);

            for (int i = 0; i < hits.Length; i++)
            {
                if (damager.transform == hits[i].transform)
                    continue;

                var enemyCharStats = hits[i].transform.GetComponentInChildren<CharacterStats>();
                if (enemyCharStats == null)
                    continue;

                enemyCharStats.Damage(_damage, damager);
                break;
            }
        }

        public void SetState(CharacterMover mover)
        {
            mover.SetState(_animState);
        }
    }

    [System.Serializable]
    public class LongBow : IAttackable
    {
        [System.Serializable]
        private class AttackInputParams
        {
            public string AttackInput = "LongbowShoot";
        }

        [SerializeField]
        private AttackInputParams _inputParams = null;
        [SerializeField]
        private GameObject _bulletPref = null;
        [SerializeField]
        private float _animState = 3f;

        private Transform _shootPoint = null;

        public void SetShootPoint(Transform shootPoint)
        {
            _shootPoint = shootPoint;
        }

        public void Attacking(Animator anim, bool isAttack)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);

            anim.SetBool(_inputParams.AttackInput, isAttack);
        }


        public void Hit(Animator anim, GameObject damager)
        {
            if (_shootPoint == null)
                return;

            var bulletObj = GameObject.Instantiate(_bulletPref);
            bulletObj.transform.position = _shootPoint.position;
            bulletObj.transform.rotation = _shootPoint.rotation;
        }

        public void SetState(CharacterMover mover)
        {
            mover.SetState(_animState);
        }
    }

    [System.Serializable]
    public class Shoot : IAttackable
    {
        [SerializeField]
        private float _animState = 0f;
        [SerializeField]
        private GameObject _bulletPref = null;
        [SerializeField]
        private float _rate = 1.5f;
        [SerializeField]
        private Vector3 _backForcePos = Vector3.zero;
        [SerializeField]
        private Vector3 _backForceRot = Vector3.zero;
        [SerializeField]
        private float _backForceSpeed = 7.5f;
        [SerializeField]
        private Transform[] _shootPoints = new Transform[0];

        [SerializeField]
        private Transform _weaponBody = null;
        [SerializeField]
        private Transform _rightHandPoint = null;
        [SerializeField]
        private Transform _leftHandPoint = null;

        [SerializeField]
        private GameObject _shootEffect = null;
        [SerializeField]
        private float _shootEffectLifeTime = 5f;

        private Vector3 _startLocalPosition = Vector3.zero;
        private Vector3 _startLocalEulerAngle = Vector3.zero;

        private float _oldShootTime = 0f;

        public void SetStartingParams(Vector3 startLocalPosition, Vector3 startLocalEulerAngle)
        {
            _startLocalPosition = startLocalPosition;
            _startLocalEulerAngle = startLocalEulerAngle;

            _oldShootTime = Time.time;
        }

        public void Attacking(Animator anim, bool isAttack)
        {
            if (_weaponBody == null)
                return;

            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
            anim.SetIKPosition(AvatarIKGoal.RightHand, _rightHandPoint.position);
            anim.SetIKPosition(AvatarIKGoal.LeftHand, _leftHandPoint.position);

            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.RightHand, _rightHandPoint.rotation);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, _leftHandPoint.rotation);

            var backForceSpeed = Time.deltaTime * _backForceSpeed;
            _weaponBody.localPosition = Vector3.Lerp(_weaponBody.localPosition, _startLocalPosition, backForceSpeed);
            _weaponBody.localRotation = Quaternion.Lerp(_weaponBody.localRotation, Quaternion.Euler(_startLocalEulerAngle), backForceSpeed);

            if (!isAttack)
                return;

            if (_rate > Time.time - _oldShootTime)
                return;

            _oldShootTime = Time.time;

            if (_shootEffect != null)
            {
                for (int i = 0; i < _shootPoints.Length; i++)
                {
                    var shootEffectObj = GameObject.Instantiate(_shootEffect);
                    shootEffectObj.transform.position = _shootPoints[i].position;
                    shootEffectObj.transform.up = _shootPoints[i].forward;
                    GameObject.Destroy(shootEffectObj, _shootEffectLifeTime);
                }
            }

            for (int i = 0; i < _shootPoints.Length; i++)
            {
                var bulletObj = GameObject.Instantiate(_bulletPref);
                bulletObj.transform.position = _shootPoints[i].position;
                bulletObj.transform.rotation = _shootPoints[i].rotation;
            }

            _weaponBody.position += _weaponBody.TransformDirection(_backForcePos);
            _weaponBody.localRotation *= Quaternion.Euler(_backForceRot);
        }

        public void Hit(Animator anim, GameObject damager)
        {
        }

        public void SetState(CharacterMover mover)
        {
            mover.SetState(_animState);
        }
    }

    [System.Serializable]
    public class EmptyWeapon : IAttackable
    {
        [SerializeField]
        private float _animState = 0f;

        public void Attacking(Animator anim, bool isAttack)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
        }

        public void Hit(Animator anim, GameObject damager)
        {
        }

        public void SetState(CharacterMover mover)
        {
            mover.SetState(_animState);
        }
    }

    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CharacterMover))]
    public class CharacterAttacker : MonoBehaviour
    {
        [SerializeField]
        private EmptyWeapon _emptyWeapon = null;

        private Animator _anim = null;
        private CharacterMover _mover = null;

        private IAttackable _weapon = null;

        private bool _isAttack = false;

        private void Start()
        {
            _anim = GetComponent<Animator>();
            _mover = GetComponent<CharacterMover>();
            SetEmptyWeapon();
        }

        public void SetInputs(bool isAttack)
        {
            _isAttack = isAttack;
        }

        private void OnAnimatorIK()
        {
            if (_weapon == null)
                return;

            _weapon.Attacking(_anim, _isAttack);
        }

        private void SendEvent()
        {
            _weapon.Hit(_anim, gameObject);
        }

        public void SetNewWeapon(IAttackable weapon)
        {
            _weapon = weapon;
            _weapon.SetState(_mover);
        }

        public void SetEmptyWeapon()
        {
            SetNewWeapon(_emptyWeapon);
        }
    }
}