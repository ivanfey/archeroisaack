﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlRandom : MonoBehaviour
{
    [SerializeField]
    private int _seed = 69228;
    private System.Random _random = null;

    [ContextMenu("ResetRandom")]
    public void ResetRandom()
    {
            _random = new System.Random(_seed);
    }
    public int Range(int a,int b)
    {
        if(_random == null)
        {
            ResetRandom();
        }

        return _random.Next(a, b);
    }

}
