﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GraphGenerator))]
[RequireComponent(typeof(ControlRandom))]

public class GraphSpawner : MonoBehaviour
{
    [SerializeField]
    private ConditionRoomSet[] _unicRooms;

    [SerializeField]
    private RoomSet _defaultRoomSet;


    [System.Serializable]
    private class ConditionRoomSet 
    {
        public RoomSet roomSet;
        public ConditionBase Condition;
        public int SpawnCount;
    }


    private void Start()
    {
        SpawnObjects();
    }

    [ContextMenu("SpawnObjects")]
    private void SpawnObjects()
    {
        var graph = GetComponent<GraphGenerator>().GenerateGraph();
        ControlRandom random= GetComponent<ControlRandom>();
        int[] unicRoomsSpawnedCounts = new int[_unicRooms.Length];


        for (int v = 0; v < graph.Vertices.Count; v++)
        {
            RoomSet currentRoomSet = null;

            for (int u = 0; u < _unicRooms.Length; u++)
            {
                bool spawnedEnough = unicRoomsSpawnedCounts[u] >= _unicRooms[u].SpawnCount;
                if (spawnedEnough) continue;

                bool canSpawn = _unicRooms[u].Condition.CanSpawnInVertex(graph, v);
                if (canSpawn)
                {
                    currentRoomSet = _unicRooms[u].roomSet;
                    unicRoomsSpawnedCounts[u]++;
                    break;
                }
            }

            if (currentRoomSet == null) currentRoomSet = _defaultRoomSet;

            GameObject vertexPrefab = currentRoomSet.Rooms[random.Range(0, currentRoomSet.Rooms.Length)];
            Instantiate(vertexPrefab, graph.Vertices[v].Position, Quaternion.identity,transform);

            for (int j = 0; j < graph.Vertices[v].Relaions.Count; j++)
            {
                int neighbourIndex = graph.Vertices[v].Relaions[j];

                Vector3 position = Vector3.zero;
                Quaternion rotation = Quaternion.identity;
                GetBridgePosition(graph.Vertices[v].Position, graph.Vertices[neighbourIndex].Position, out position, out rotation);

                GameObject relationPrefab = currentRoomSet.Bridges[random.Range(0, currentRoomSet.Bridges.Length)];
                Instantiate(relationPrefab, position,rotation,transform);
            }
        }
    }

    


    private void GetBridgePosition(Vector3 p1,Vector3 p2, out Vector3 position, out Quaternion rotation)
    {
        position = (p1 + p2) * 0.5f;
        rotation = Quaternion.LookRotation(p2 - p1, Vector3.up);
    }

 
}
