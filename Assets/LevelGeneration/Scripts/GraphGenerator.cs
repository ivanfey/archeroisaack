﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ControlRandom))]
public class GraphGenerator : MonoBehaviour
{
    private Graph _currentGraph;

    [SerializeField]
    private int _criticalPathLength = 10;
    [SerializeField]
    private int _subBranchesCount = 3;
    [SerializeField]
    private Vector3[] _possibleDirections = new Vector3[3];
    [SerializeField]
    private float _stepDistance = 10f;
    [SerializeField]
    private float epsilion = 1f;

    private ControlRandom _random;

    [ContextMenu("GenerateDebug")]
    private void GenerateDebug()
    {
        _currentGraph = GenerateGraph();
    }

    public Graph GenerateGraph()
    {
        Graph result = new Graph();
        _random = GetComponent<ControlRandom>();

        result.Vertices.Add(new GraphVertex());
        for (int i = 1; i < _criticalPathLength; i++)
        {
            Vector3 direction = _possibleDirections[_random.Range(0, _possibleDirections.Length)];
            Vector3 position = PointFromDirection(direction, result.Vertices[i - 1].Position, _stepDistance);
            result.Vertices.Add(new GraphVertex(position, i - 1));
        }
        result.AddInfo("BossRoom", _criticalPathLength - 1);


        for (int i = 0; i < _subBranchesCount; i++)
        {
            int vertexToConnect = _random.Range(0, result.Vertices.Count);
            Vector3 newPosition = -Vector3.one;

            while (!HaveEmptyPostion(vertexToConnect,result,_possibleDirections, _random, ref newPosition,epsilion))
            {
                vertexToConnect = (vertexToConnect + 1) % result.Vertices.Count;
            }

            result.Vertices.Add(new GraphVertex(newPosition, vertexToConnect));

        }

        return result;
    }

    private bool HaveEmptyPostion(int vertexIndex,Graph graph,Vector3[] possibleDirections,ControlRandom random ,ref Vector3 emptyPosition, float epsilion)
    {
        int randomDirection = random.Range(0, possibleDirections.Length);

        for (int i = 0; i < possibleDirections.Length; i++)
        {
            Vector3 direction = _possibleDirections[randomDirection];
            Vector3 position = PointFromDirection(direction, graph.Vertices[vertexIndex].Position, _stepDistance);

            if (IsEmpty(position, graph, epsilion))
            {
                emptyPosition = position;
                return true;
            }
            else
            {
                randomDirection = (randomDirection + 1) % possibleDirections.Length;
                continue;
            }
        }
        return false;
    }

    private bool IsEmpty(Vector3 point, Graph graph, float epsilion)
    {
        for (int i = 0; i < graph.Vertices.Count; i++)
        {
            float sqrDist = (point - graph.Vertices[i].Position).sqrMagnitude;
            if (sqrDist < epsilion * epsilion) return false;
        }
        return true;
    }

    private Vector3 PointFromDirection(Vector3 direction, Vector3 origin, float distance)
    {
        Vector3 result = direction;
        result = result.normalized * distance;
        result += origin;

        return result;
    }

   
    private void OnDrawGizmosSelected()
    {
        //draw directions
        Gizmos.color =Color.Lerp( Color.red,Color.yellow,0.5f);
        for (int i = 0; i < _possibleDirections.Length; i++)
        {
            Gizmos.DrawLine(Vector3.zero, PointFromDirection(_possibleDirections[i], Vector3.zero, _stepDistance));
        }

        //draw epsilion
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(Vector3.zero, epsilion);
        for (int i = 0; i < _possibleDirections.Length; i++)
        {
            Gizmos.DrawWireSphere(PointFromDirection(_possibleDirections[i], Vector3.zero, _stepDistance),epsilion);
        }

        if (_currentGraph == null || _currentGraph.Vertices.Count == 0) return;

        for (int i = 0; i < _currentGraph.Vertices.Count; i++)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(_currentGraph.Vertices[i].Position, 1f);

            for (int j = 0; j < _currentGraph.Vertices[i].Relaions.Count; j++)
            {
                Gizmos.color = Color.red;
                int neighbourIndex = _currentGraph.Vertices[i].Relaions[j];
                Gizmos.DrawLine(_currentGraph.Vertices[i].Position, _currentGraph.Vertices[neighbourIndex].Position);
            }
        }
    }
}

public class Graph
{
    public List<GraphVertex> Vertices;
    private Dictionary<string, object> _subInfo;

    public void AddInfo(string name, object contains)
    {
        bool haveKey = _subInfo.ContainsKey(name);
        if (haveKey)
        {
            Debug.LogError("Try to add existing info, key " + name);
            return;
        }
        _subInfo.Add(name, contains);
    }

    public object GetInfo(string name)
    {
        bool haveInfo = _subInfo.TryGetValue(name, out object contains);
        if (!haveInfo)
        {
            Debug.LogError("Try to get nonExisting info, key " + name);
        }
        return contains;
    }

    public bool IsConnected(int v1, int v2)
    {
        return Vertices[v1].Relaions.Contains(v2) || Vertices[v2].Relaions.Contains(v1);
    }

    public Graph()
    {
        Vertices = new List<GraphVertex>();
        _subInfo = new Dictionary<string, object>();
    }
}

public class GraphVertex
{
    public Vector3 Position;
    public List<int> Relaions;

    public GraphVertex()
    {
        Position = Vector3.zero;
        Relaions = new List<int>();
    }
    public GraphVertex(Vector3 position)
    {
        Position = position;
        Relaions = new List<int>();
    }

    public GraphVertex(Vector3 position, params int[] relations)
    {
        Position = position;
        Relaions = new List<int>();
        Relaions.AddRange(relations);
    }


}
