﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Conditions/Is start room")]
public class ConditionIsStartRoom : ConditionBase
{
    public override bool CanSpawnInVertex(Graph graph, int vertex)
    {
        return vertex==0;
    }
}
