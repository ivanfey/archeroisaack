﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Conditions/Is dead end")]
public class ConditionIsDeadEnd : ConditionBase
{
    public override bool CanSpawnInVertex(Graph graph, int vertex)
    {
        int relationsCount = 0;

        for (int i = 0; i < graph.Vertices.Count; i++)
        {
            relationsCount+= graph.IsConnected(vertex,i) ? 1 : 0;
            if (relationsCount > 1) return false;
        }
        return true;
    }
}
