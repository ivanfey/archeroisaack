﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Conditions/Is boss room")]
public class ConditionIsBossRoom : ConditionBase
{
    public override bool CanSpawnInVertex(Graph graph, int vertex)
    {
        object info = graph.GetInfo("BossRoom");
        int bossRoomIndex = (int)info;

        return vertex == bossRoomIndex;
    }
}
