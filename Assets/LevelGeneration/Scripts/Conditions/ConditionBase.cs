﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionBase : ScriptableObject
{
    public virtual bool CanSpawnInVertex(Graph graph, int vertex)
    {
        return false;
    }
}
