﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Create RoomSet")]
public class RoomSet : ScriptableObject
{
    public GameObject[] Rooms;
    public GameObject[] Bridges;
}
