﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LightmapPrefab : MonoBehaviour 
{

	Renderer[] targetRenderers;
	public LightmapBase[] targetLightmaps;

	public int lightmapID;

	void Start () 
	{
		UpdateLightmap();
	}

	void Update()
	{
		if(!Application.isPlaying)
			UpdateLightmap();
	}

	void UpdateLightmap()
	{
		LightmapManager manager = LightmapManager.Instance;
		if(manager == null) manager = GameObject.FindObjectOfType<LightmapManager>();
		if (manager == null) return;

		PrefabLightData data = new PrefabLightData();
		if (!manager.GetLightData(lightmapID, ref data)) return;

		Renderer[] currentRenderers = GetComponentsInChildren<Renderer>();

		for (int a = 0; a < data.lightmapIndexes.Length; a++)
		{
			currentRenderers[a].lightmapIndex = data.lightmapIndexes[a];
			currentRenderers[a].lightmapScaleOffset = data.uvOffsets[a];
		}
	}

	void UpdateLightmapOld()
	{

		targetLightmaps = GameObject.FindObjectsOfType<LightmapBase>();

		foreach(LightmapBase l in targetLightmaps)
		{

			if (l.lightmapId == lightmapID)
			{
				Renderer[] targetRenderers = l.GetComponentsInChildren<Renderer>();
				Renderer[] currentRenderers = GetComponentsInChildren<Renderer>();

				for(int a = 0;a<targetRenderers.Length;a++)
				{
					currentRenderers[a].lightmapIndex = targetRenderers[a].lightmapIndex;
					currentRenderers[a].lightmapScaleOffset = targetRenderers[a].lightmapScaleOffset;
				}

			}

		}
	}

}