﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(LightmapManager))]
public class LightmapManagerEditor : Editor
{
    LightmapManager curManager;

    public override void OnInspectorGUI()
    {
        curManager = (LightmapManager)target;

        base.OnInspectorGUI();

        GUILayout.Label("Info:", EditorStyles.boldLabel);

        string haveData = (curManager.HaveData) ? "Have Data" : "No Data";
        EditorGUILayout.LabelField(haveData);
        EditorGUILayout.LabelField("Data Count:" + curManager.dataCount);



        if (GUILayout.Button("Create Lightdata"))
        {
            curManager.CreateLightData();
        }

        EditorUtility.SetDirty(curManager);


    }

}
