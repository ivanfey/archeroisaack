﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightmapManager : MonoBehaviour
{
    public static LightmapManager Instance = null;

    private void OnEnable()
    {
        Instance = this;
    }

    private void OnDisable()
    {
        Instance = null;
    }


    [SerializeField]
    PrefabLightData[] myData = null;
    
    public bool HaveData
    {
        get
        {
            return myData != null;
        }
    }

    public int dataCount
    {
        get
        {
            if (!HaveData) return 0;
            return myData.Length;
        }
    }

    [ContextMenu("CreateLightData")]
    public void CreateLightData()
    {
        var LightmapBases = GameObject.FindObjectsOfType<LightmapBase>();

        myData = new PrefabLightData[LightmapBases.Length];

        for (int i = 0; i < myData.Length; i++)
        {
            myData[i] = LightmapBases[i].prefabLightData;
        }
    }

    public bool GetLightData(int index, ref int[] lightmapIndexes,ref Vector4[] uvOffsets)
    {
        PrefabLightData data = new PrefabLightData();
        if (!GetDataByID(index, ref data)) return false;

        lightmapIndexes = data.lightmapIndexes;
        uvOffsets = data.uvOffsets;
        return true;
    }

    public bool GetLightData(int index, ref PrefabLightData data)
    {
        if (!GetDataByID(index, ref data)) return false;

        return true;
    }

    bool GetDataByID(int id, ref PrefabLightData prefabLightData)
    {
        if (myData == null) return false;

        for (int i = 0; i < myData.Length; i++)
        {
            if (myData[i].index == id) {
                prefabLightData = myData[i];
                return true;
            }
        }
        return false;
    }


}

[System.Serializable]
public struct PrefabLightData
{
    public int index;
    public int[] lightmapIndexes;
    public Vector4[] uvOffsets;

    public PrefabLightData(int index, int[] lightmapIndexes, Vector4[] uvOffsets)
    {
        this.index = index;
        this.lightmapIndexes = lightmapIndexes;
        this.uvOffsets = uvOffsets;
    }
}
