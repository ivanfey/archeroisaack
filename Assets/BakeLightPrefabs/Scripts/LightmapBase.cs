﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightmapBase : MonoBehaviour {

	public int lightmapId;

	public PrefabLightData prefabLightData
	{
		get
		{
			Renderer[] targetRenderers = GetComponentsInChildren<Renderer>();
			int[] lightmapIndexes = new int[targetRenderers.Length];
			Vector4[] uvOffsets = new Vector4[targetRenderers.Length];

			for (int i = 0; i < targetRenderers.Length; i++)
			{
				lightmapIndexes[i] = targetRenderers[i].lightmapIndex;
				uvOffsets[i] = targetRenderers[i].lightmapScaleOffset;
			}


			return new PrefabLightData(lightmapId, lightmapIndexes, uvOffsets);
		}
	}
}
